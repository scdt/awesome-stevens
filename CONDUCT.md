# Code of Conduct

## Our Pledge

We will record the best sites that Stevens has to offer (and that we know 
about). We will keep this list as light-hearted as possible.

## Our Standards

Examples of acceptable behavior include:

1. Submitting witty sites made by a Stevens student.
2. Submitting a satirical site devoted to Stevens/Snevets-based content.

Examples of unacceptable behavior include:

1. Normie sites. The website must have a joke to it, or some quality which makes
   it exceptionally interesting.
2. Submitting a site written by someone at MIT, RIT, or (*cringe*) NJIT.

## Enforcement

Failure to respect this CoC and the contributing guidelines may result in
permanent banning from involvement with this project.