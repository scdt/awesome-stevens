# Contributing

The [Awesome Stevens](https://gitlab.com/scdt/awesome-stevens) project is meant 
to highlight the domains and projects made by Stevens students, especially those
made for the meme.

## Pull Request Process

1. Come up with an interesting domain, and maybe something to put on it.
2. Submit a PR asking to have your domain featured.